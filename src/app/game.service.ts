import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private playOptions: string[];
  private playerChoice: string;
  private computerChoice: string;
  private result: string;

  constructor() {
    this.playOptions = ['Rock', 'Paper', 'Sciccors'];
  }

  public play(playerChoice: string): void {
    this.playerChoice = playerChoice;
    this.computerPlays();
    this.result = this.whoWins();
  }

  public getPlayOptions(): Array<string> {
    return this.playOptions;
  }

  public getComputer(): string {
    return this.computerChoice;
  }

  public getResult(): string {
    return this.result;
  }

  private computerPlays() {
    const rand = Math.floor(Math.random() * this.playOptions.length);
    this.computerChoice = this.playOptions[rand];
  }

  private whoWins() {
    if (this.computerChoice === this.playerChoice) {
      return "it´s a draw!";
    }
    else if
      ((this.computerChoice === this.playOptions[0] && this.playerChoice === this.playOptions[1]) ||
      (this.computerChoice === this.playOptions[1] && this.playerChoice === this.playOptions[2]) ||
      (this.computerChoice === this.playOptions[2] && this.playerChoice === this.playOptions[0]))
      return "You win!";
    else {
      return "You Lose!";
    }
  }
}
