import { Component } from '@angular/core';
import { GameService } from '../game.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private game: GameService) {}

  check(event) {
    this.game.play(event.target.textContent.trim());
  }
}
